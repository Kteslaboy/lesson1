package com.example.kate0.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import static android.app.Activity.RESULT_OK;

public class AddActivity extends AppCompatActivity{
    public static final String EXTRA_TEXT = "rrgrg";
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanseState){
        super.onCreate(savedInstanseState);
        setContentView(R.layout.activity_add);
        editText = findViewById(R.id.input);

        findViewById(R.id.action).setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.putExtra(EXTRA_TEXT, editText.getText().toString());
                        setResult(RESULT_OK, intent);
                        onBackPressed();
                    }
                });
        }
    }
